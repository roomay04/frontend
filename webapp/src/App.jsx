import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import NavbarInstance from './components/NavbarInstance.jsx';
import SearchBar from './components/SearchBar.jsx';
import CardList from './components/CardList';
var axios = require('axios');

class App extends Component {
  //ini adalah fungsi yang bakal dilakuin sebelum di mount
  componentWillMount(){
    // this.username.map( username => axios.get(`https://api.github.com/users/${username}`).then(resp =>this.addNewCard(resp.data))); 
  }

  //ini nantinya bakal ngakses tiap bengkel dari database
  // username= ['hanifaarrumaisha','dharmawankenny','denipramulia','jordwalke']
  constructor(props){
    super(props)
    this.state = {
      cards: [ ]
    }
    this.addNewCard = (cardInfo) => {
      this.setState(prevState => ({cards: prevState.cards.concat(cardInfo)}));
    }
  }

  

  render() {
    return (
      <div className="App">
        <NavbarInstance />
        <SearchBar onSubmit={this.addNewCard}/>
        <CardList cards={this.state.cards} />
      </div>
    );
  }
}

export default App;
