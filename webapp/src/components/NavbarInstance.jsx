import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Navbar,Nav,NavItem, MenuItem, NavDropdown, Button} from 'react-bootstrap';
import './NavbarInstance.css';

export default class NavbarInstance extends Component{
  render(){
    return(
      <div className="container">
        <Navbar bsStyle="default">
          <div className="container-fluid">
            <Navbar.Header>
              <Navbar.Toggle type="button" data-toggle="collapse" data-target="#navbar">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </Navbar.Toggle>
              <a id="navbar-brand" href="http://disputebills.com"><img src="http://www.goodmanmfg.com/Portals/0/images/Logos/BBB%20Logo-tall.jpg" alt="Dispute Bills" /></a>
            </Navbar.Header>
            
      
            
            <Navbar.Collapse id="navbar" >
              <Nav pullLeft>
                <NavItem className="active" href="#">Home</NavItem>
                <NavItem href="#">About</NavItem>
              </Nav>
              <Nav pullRight>
                <NavItem className="active" href="#">Home</NavItem>
                <NavItem href="#">About</NavItem>
              </Nav>
            </Navbar.Collapse>
          </div>
        </Navbar>
      </div>
    );
  }
}

  