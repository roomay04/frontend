import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Card from './Card';
import {Grid, Row, Col, Clearfix} from 'react-bootstrap';

// const Card = (props) =>{
// 	return (
//   	<div style={{margin:'1em'}}>
//       <img width='75' src={props.avatar_url} alt="hehe"/>
//       <div style={{display:'inline-block'}}>
//         <div style={{fontSize:'1.25em', fontWeight:'bold'}}>{props.name}</div>
//         <div>{props.company}</div>
//       </div>
//     </div>
//   );
// };

const CardList = (props) => {
	return (
    <Grid>
      <Row className="show-grid">
        {props.cards.map(card => <Card key={card.id} {...card}/>)}
      </Row>
    </Grid>
  )
};

export default CardList;